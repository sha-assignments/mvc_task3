package MVC;

public class MultiLinugualStringTable {
	private enum LanguageSetting {
		English, Dutch
	}

	private static LanguageSetting cl = LanguageSetting.English;
	private static String[] em = { "Enter your name:", "Welcome", "Have a good time playing Abominodo" };
	private static String[] dm = { "Voer je naam in:", "Welkom", "Heb een goede tijd met het spelen van Abominodo" };

	public static String getMessage(int index) {
		if (cl == LanguageSetting.English) {
			return em[index];
		} else {
			return dm[index];
		}

	}
}
